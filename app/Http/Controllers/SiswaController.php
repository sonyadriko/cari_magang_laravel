<?php

namespace App\Http\Controllers;

use App\MSiswaModel;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MSiswaModel  $mSiswaModel
     * @return \Illuminate\Http\Response
     */
    public function show(MSiswaModel $mSiswaModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MSiswaModel  $mSiswaModel
     * @return \Illuminate\Http\Response
     */
    public function edit(MSiswaModel $mSiswaModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MSiswaModel  $mSiswaModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MSiswaModel $mSiswaModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MSiswaModel  $mSiswaModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(MSiswaModel $mSiswaModel)
    {
        //
    }
}
